//
//  DMServer.m
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import "DMServer.h"
#import <sys/socket.h>
#import <netinet/in.h>

#import "DMServerConnection.h"

#define BACKLOG 5

@implementation DMServer
{
	CFSocketNativeHandle _hostFileDescriptor;
	struct sockaddr_in _serverAddress;
	in_port_t _port;
	CFSocketRef _socket;
	uint32_t _protocolFamily;
	
	NSMutableArray *_connections;
}

- (void) handleNewConnectionWithDescriptor:(CFSocketNativeHandle)descriptor
{
	// Create a new connection with the file descriptor and close once completed
	DMServerConnection *connection = [[DMServerConnection alloc] initWithDescriptor:descriptor
																	completionBlock:^(DMServerConnection *completedConnection) {
//		close(descriptor);
		[_connections removeObject:completedConnection];
	}];
	
//	[DMServerConnection handleConnection:descriptor fromSever:self];
	
	// Retain the object by adding it to the array
	[_connections addObject:connection];
}

// This function is called by CFSocket when a new connection comes in.
// We gather some data here, and convert the function call to a method
// invocation on TCPServer.
static void TCPServerAcceptCallBack(CFSocketRef socket, CFSocketCallBackType type, CFDataRef address, const void *data, void *info)
{
	DMServer *server = (__bridge DMServer *)info;
	if (kCFSocketAcceptCallBack == type)
	{
		// for an AcceptCallBack, the data parameter is a pointer to a CFSocketNativeHandle
		CFSocketNativeHandle nativeSocketHandle = *(CFSocketNativeHandle *)data;
		uint8_t name[SOCK_MAXADDRLEN];
		socklen_t namelen = sizeof(name);
		NSData *peer = nil;
		if (0 == getpeername(nativeSocketHandle, (struct sockaddr *)name, &namelen))
		{
			peer = [NSData dataWithBytes:name length:namelen];
		}
		
		[server handleNewConnectionWithDescriptor:nativeSocketHandle];
	}
}

- (void) finishedWithConnection:(DMServerConnection *)connection
{
	[_connections removeObject:connection];
}


- (id) initWithTCPPort:(in_port_t)port
{
	if (self = [super init])
	{
		_connections = [NSMutableArray array];
		_port = port;
		
		CFSocketContext socketCtx = {0, (__bridge void*)self, NULL, NULL, NULL};
		
		// Start by trying to do everything with IPv6.  This will work for both IPv4 and IPv6 clients 
		// via the miracle of mapped IPv4 addresses.
        _socket = CFSocketCreate(kCFAllocatorDefault, PF_INET6, SOCK_STREAM, IPPROTO_TCP, kCFSocketAcceptCallBack, (CFSocketCallBack)&TCPServerAcceptCallBack, &socketCtx);
		
		if (_socket)
		{
			// Successfully created IPv6 address
			_protocolFamily = PF_INET6;
		}
		else
		{
			// Error create IPv6 socket so create v4
			_socket = CFSocketCreate(kCFAllocatorDefault, PF_INET, SOCK_STREAM, IPPROTO_TCP, kCFSocketAcceptCallBack, (CFSocketCallBack)&TCPServerAcceptCallBack, &socketCtx);
			if (_socket)
				_protocolFamily = PF_INET;
		}
		
		NSData *addressData = nil;
		
		// Set up the IP endpoint
		if (_protocolFamily == PF_INET6)
		{
			struct sockaddr_in6 addr6;
			memset(&addr6, 0, sizeof(addr6));
			addr6.sin6_len = sizeof(addr6);
			addr6.sin6_family = AF_INET6;
			addr6.sin6_port = htons(_port);
			addr6.sin6_flowinfo = 0;
			addr6.sin6_addr = in6addr_any;
			
			addressData = [NSData dataWithBytes:&addr6 length:sizeof(addr6)];
		}
		else
		{
			struct sockaddr_in addr4;
			memset(&addr4, 0, sizeof(addr4));
			addr4.sin_len = sizeof(addr4);
			addr4.sin_family = AF_INET;
			addr4.sin_port = htons(_port);
			addr4.sin_addr.s_addr = htonl(INADDR_ANY);
			
			addressData = [NSData dataWithBytes:&addr4 length:sizeof(addr4)];
		}
		
		if (kCFSocketSuccess != CFSocketSetAddress(_socket, (__bridge CFDataRef)addressData)) {
			NSLog(@"Failed v4");
		}
		
		int yes = 1;
		setsockopt(CFSocketGetNative(_socket), SOL_SOCKET, SO_REUSEADDR, (void *)&yes, sizeof(yes));
		
		// set up the run loop sources for the sockets
		CFRunLoopRef cfrl = CFRunLoopGetCurrent();
		CFRunLoopSourceRef source = CFSocketCreateRunLoopSource(kCFAllocatorDefault, _socket, 0);
		CFRunLoopAddSource(cfrl, source, kCFRunLoopCommonModes);
		if (source) CFRelease(source);
		
		NSLog(@"Listening on port %i", _port);
	}
	
	return self;
}

- (void) dealloc
{
	if (_socket) CFRelease(_socket);
}

@end
