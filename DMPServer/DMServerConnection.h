//
//  DMServerConnection.h
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DMServer;

@interface DMServerConnection : NSObject

- (id) initWithDescriptor:(CFSocketNativeHandle)descriptor completionBlock:(void(^)(DMServerConnection *))block;
//+ (void) handleConnection:(CFSocketNativeHandle)descriptor fromSever:(DMServer *)server;

@end
