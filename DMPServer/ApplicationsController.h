//
//  DMApplicationsController.h
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import "DMApiController.h"

@interface ApplicationsController : DMApiController

@end
