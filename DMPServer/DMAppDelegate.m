//
//  DMAppDelegate.m
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import "DMAppDelegate.h"
#import "DMServer.h"

@implementation DMAppDelegate
{
	DMServer *_server;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	_server = [[DMServer alloc] initWithTCPPort:50000];
}

- (void) applicationWillTerminate:(NSNotification *)notification
{
	_server = nil;
}

@end
