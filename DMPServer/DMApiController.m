//
//  DMApiController.m
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import "DMApiController.h"
#import "DMHttpRequest.h"
#import "DMHttpResponse.h"
#import "DMApiRoute.h"

#ifdef DEBUG
#define DEBUG_LOG(a) NSLog(@"%@", a)
#else
#endif

@implementation DMApiController

+ (NSData *) createUnfoundResponse
{
	DMHttpResponse *httpResponse = [[DMHttpResponse alloc] init];
	httpResponse.statusCode = kHttpResponseNotFound;
	[httpResponse setBody:[@"404 Not Found" dataUsingEncoding:NSASCIIStringEncoding] withContentType:@"text/html"];
	
	return httpResponse.responseData;
}

+ (NSData *) respondToRequest:(DMHttpRequest *)request
{
	NSData *response = nil;
	
	DMApiRoute *requestRoute = [[DMApiRoute alloc] initWithRequest:request];
	
	NSString *controllerName = requestRoute.controllerName;
#ifdef DEBUG
	NSLog(@"%@", controllerName);
#endif
	
	Class controllerClass = NSClassFromString(controllerName);
	
	// Check whether request calls an applicable controller
	if ([controllerClass isSubclassOfClass:[DMApiController class]])
	{
		// Get request type to determine method call
		NSString *requestMethod = requestRoute.requestMethod;
		DEBUG_LOG(requestMethod);
		SEL requestSelector = NSSelectorFromString(requestMethod);
		
		id firstParameter = requestRoute.firstParameter;
		
		// Find whether the class responds to the selector
		NSMethodSignature *methodSig = [controllerClass methodSignatureForSelector:requestSelector];
		if (methodSig)
		{
			DMHttpResponse *httpResponse = [[DMHttpResponse alloc] initWithRequest:request];
			id returnValue = nil;
			
			// Create the invocation with the method signature and requested selector
			NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:methodSig];
			invocation.selector = requestSelector;
			
			if (firstParameter && methodSig.numberOfArguments > 1)		// ??? this may be 3 if self and cmd are counted
			{
				NSString *parameterType = [NSString stringWithUTF8String:[methodSig getArgumentTypeAtIndex:2]];
				if ([parameterType isEqualToString:@"@"])	// Use the string
				{
					NSString *arg = [firstParameter stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
					[invocation setArgument:&arg atIndex:2];
				}
				else			// send the integer value
				{
					int num = [firstParameter intValue];
					[invocation setArgument:&num atIndex:2];
				}
			}
			
			// Invoke the method and get the return value
			[invocation invokeWithTarget:controllerClass];
			[invocation getReturnValue:&returnValue];
			DEBUG_LOG(returnValue);
			
			httpResponse.statusCode = kHttpResponseSuccess;
			
			// Populate body of response
			if ([returnValue isKindOfClass:[NSDictionary class]] || [returnValue isKindOfClass:[NSArray class]])	// JSON
			{
				NSData *json = [NSJSONSerialization dataWithJSONObject:returnValue options:0 error:nil];
				[httpResponse setBody:json withContentType:@"application/json"];
			}
			else if ([returnValue isKindOfClass:[NSString class]])		// HTML
			{
				NSData *data = [returnValue dataUsingEncoding:NSASCIIStringEncoding];
				[httpResponse setBody:data withContentType:@"text/html"];
			}
			
			// get the data to write
			response = httpResponse.responseData;
		}
	}
	
	// Return 404 if nothing found
	if (!response)
		response = [DMApiController createUnfoundResponse];
	
	return response;
}

@end
