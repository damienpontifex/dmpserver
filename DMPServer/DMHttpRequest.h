//
//  DMHttpRequest.h
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMHttpRequest : NSObject

- (id) initWithData:(NSData *)data;

@property (strong, nonatomic, readonly) NSString *requestPath;
@property (strong, nonatomic, readonly) NSString *requestType;

- (NSString *) requestHeader:(NSString *)header;

@end
