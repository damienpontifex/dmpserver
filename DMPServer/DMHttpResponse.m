//
//  DMHttpResponse.m
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import "DMHttpResponse.h"

@implementation DMHttpResponse
{
	CFHTTPMessageRef _message;
}

@synthesize statusCode = _statusCode;
@synthesize responseData = _responseData;
@synthesize originalRequest = _originalRequest;

- (void) setStatusCode:(int)status
{
	_statusCode = status;
	_message = CFHTTPMessageCreateResponse(kCFAllocatorDefault, _statusCode, NULL, kCFHTTPVersion1_1);
}

- (void) setBody:(NSData *)body withContentType:(NSString *)contentType
{
	CFHTTPMessageSetBody(_message, (__bridge_retained CFDataRef)body);
	CFHTTPMessageSetHeaderFieldValue(_message, (CFStringRef)@"Content-Type", (__bridge CFStringRef)contentType);
	CFHTTPMessageSetHeaderFieldValue(_message, (CFStringRef)@"Content-Length", (__bridge CFStringRef)[NSString stringWithFormat:@"%lu", body.length]);
}

- (NSData *) responseData
{
	if (_message)
		return (__bridge_transfer NSData *)(CFHTTPMessageCopySerializedMessage(_message));
	else 
		return nil;
}

- (id) initWithRequest:(DMHttpRequest *)request
{
	if (self = [super init])
	{
		_originalRequest = request;
	}
	
	return self;
}

@end
