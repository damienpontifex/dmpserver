//
//  DMHttpRequest.m
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import "DMHttpRequest.h"

@implementation DMHttpRequest
{
	CFHTTPMessageRef _message;
}

@synthesize requestPath = _requestPath;
@synthesize requestType = _requestType;

- (NSString *) requestPath
{
	if (!_requestPath)
	{
		NSURL *url = (__bridge_transfer NSURL *)CFHTTPMessageCopyRequestURL(_message);
		_requestPath = url.path;
	}
	
	return _requestPath;
}

- (NSString *) requestType
{
	if (!_requestType)
	{
		NSString *typeString = (__bridge_transfer NSString *)CFHTTPMessageCopyRequestMethod(_message);
		_requestType = typeString.lowercaseString;
	}
	
	return _requestType;
}

- (NSString *) requestHeader:(NSString *)header
{
	NSString *returnHeader = (__bridge_transfer NSString *)CFHTTPMessageCopyHeaderFieldValue(_message, (__bridge CFStringRef)(header));
	
	return returnHeader;
}

- (id) initWithData:(NSData *)data
{
	if (self = [super init])
	{
		_message = CFHTTPMessageCreateEmpty(kCFAllocatorDefault, TRUE);
		
		Boolean success = CFHTTPMessageAppendBytes(_message, data.bytes, data.length);
		if (!success && CFHTTPMessageIsHeaderComplete(_message))
		{
			if (_message) CFRelease(_message);
			return nil;
		}
	}
	
	return self;
}

@end
