//
//  HomeController.m
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import "HomeController.h"

@implementation HomeController

+ (NSString *) get
{
	return @"Got Home";
}

+ (NSString *) getNumber:(int)num
{
	return [NSString stringWithFormat:@"Got Number: %i", num];
}

+ (NSString *) getString:(NSString *)string
{
	return [NSString stringWithFormat:@"Got string: %@", string];
}

@end
