//
//  DMHttpResponse.h
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#define kHttpResponseSuccess 200
#define kHttpResponseNotFound 404
#define kHttpResponseServerError 400

#import <Foundation/Foundation.h>

@class DMHttpRequest;

@interface DMHttpResponse : NSObject

@property (nonatomic) int statusCode;

- (void) setBody:(NSData *)body withContentType:(NSString *)contentType;

@property (strong, nonatomic, readonly) NSData *responseData;
@property (strong, nonatomic, readonly) DMHttpRequest *originalRequest;

- (id) initWithRequest:(DMHttpRequest *)request;

@end
