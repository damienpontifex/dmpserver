//
//  main.m
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}
