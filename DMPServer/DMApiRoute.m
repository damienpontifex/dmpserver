//
//  DMApiRoute.m
//  DMPServer
//
//  Created by Damien Pontifex on 14/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import "DMApiRoute.h"
#import "DMHttpRequest.h"

@implementation DMApiRoute
{
	NSArray *_pathComponents;
	NSString *_requestType;
}

- (id) initWithRequest:(DMHttpRequest *)request
{
	if (self = [super init])
	{
		_pathComponents = request.requestPath.pathComponents;
		_requestType = request.requestType.lowercaseString;
	}
	
	return self;
}

- (NSString *) controllerName
{
	NSString *controller = nil;
	// Get controller name with uppercase first letter
	if (_pathComponents.count > 1)
	{
		controller = [[_pathComponents objectAtIndex:1] lowercaseString];
		controller = [controller stringByReplacingCharactersInRange:NSMakeRange(0, 1)
																 withString:[controller substringToIndex:1].uppercaseString];
	}
	else
		controller = @"Home";
	
	controller = [controller stringByAppendingString:@"Controller"];
	
	return controller;
}

- (NSString *) requestMethod
{
	// Get request type to determine method call
	NSString *method = _requestType;
	
	id firstParameter = nil;
	
	if (_pathComponents.count > 2)
	{
		NSString *detailMethod = [[_pathComponents objectAtIndex:2] lowercaseString];
		detailMethod = [detailMethod stringByReplacingCharactersInRange:NSMakeRange(0, 1)
															 withString:[detailMethod substringToIndex:1].uppercaseString];
		
		method = [method stringByAppendingFormat:@"%@:", detailMethod];
		
		if (_pathComponents.count > 3)
			firstParameter = [_pathComponents objectAtIndex:3];
	}
	
	return method;
}

- (id) firstParameter
{
	if (_pathComponents.count > 3)
		return [_pathComponents objectAtIndex:3];
	else
		return nil;
}

@end
