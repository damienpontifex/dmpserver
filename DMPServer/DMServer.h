//
//  DMServer.h
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DMServerConnection;

@interface DMServer : NSObject

- (id) initWithTCPPort:(in_port_t)port;
- (void) finishedWithConnection:(DMServerConnection *)connection;

@end
