//
//  DMApplicationsController.m
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import "ApplicationsController.h"

@implementation ApplicationsController

+ (NSArray *) get
{
	NSArray *allApps = nil;
	
	NSFileManager *fm = [[NSFileManager alloc] init];
	
	allApps = [fm contentsOfDirectoryAtPath:@"/Applications" error:nil];
	
	return allApps;
}

@end
