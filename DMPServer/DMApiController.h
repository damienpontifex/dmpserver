//
//  DMApiController.h
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import <Foundation/Foundation.h>


@class DMHttpRequest;

@interface DMApiController : NSObject

+ (NSData *) respondToRequest:(DMHttpRequest *)request;

@end
