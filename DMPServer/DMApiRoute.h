//
//  DMApiRoute.h
//  DMPServer
//
//  Created by Damien Pontifex on 14/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DMHttpRequest;

@interface DMApiRoute : NSObject

- (id) initWithRequest:(DMHttpRequest *)request;

@property (nonatomic, readonly) NSString *controllerName;
@property (nonatomic, readonly) NSString *requestMethod;
@property (nonatomic, readonly) id firstParameter;

@end
