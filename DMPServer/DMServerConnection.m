//
//  DMServerConnection.m
//  DMPServer
//
//  Created by Damien Pontifex on 9/06/12.
//  Copyright (c) 2012 OrionTechnology. All rights reserved.
//

#import "DMServerConnection.h"
#import <sys/socket.h>

#import "DMHttpRequest.h"
#import "DMApiController.h"
#import "DMServer.h"

@interface DMServerConnection ()
{
	CFSocketNativeHandle _descriptor;
	
	NSFileHandle *_handle;
}

@property (copy, nonatomic) void(^finishedWithConnection)(DMServerConnection *connection);

- (void) closeConnection;

@end



@implementation DMServerConnection

@synthesize finishedWithConnection = _finishedWithConnection;

- (void) respondToRequest:(DMHttpRequest *)request
{
	NSData *responseData = nil;
	
	responseData = [DMApiController respondToRequest:request];
	
	[_handle writeData:responseData];
}

- (void) receiveMessage:(NSNotification *)notification
{
	NSLog(@"New Connection");
	NSData *data = [notification.userInfo objectForKey:NSFileHandleNotificationDataItem];
	
	if (data.length == 0)
	{
		[_handle readInBackgroundAndNotify];
		return;
	}
	
	if (data)
	{
		dispatch_queue_t queue = dispatch_queue_create("com.connection.queue", NULL);
		dispatch_async(queue, ^{
			DMHttpRequest *request = [[DMHttpRequest alloc] initWithData:data];
			[self respondToRequest:request];
			[self closeConnection];
		});
	}
}

#pragma ===================================================================
#pragma mark - Lifecycle
#pragma ===================================================================

- (void) closeConnection
{
	if (_finishedWithConnection)
		_finishedWithConnection(self);
}

//+ (void) handleConnection:(CFSocketNativeHandle)descriptor fromSever:(DMServer *)server

- (id) initWithDescriptor:(CFSocketNativeHandle)descriptor completionBlock:(void(^)(DMServerConnection *))block
{
    self = [super init];
    if (self)
	{
        self.finishedWithConnection = block;
		_descriptor = descriptor;
//		
//	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//		
//		// Create the IO Queue
//		dispatch_io_t inQ = dispatch_io_create(DISPATCH_IO_STREAM, descriptor, queue, ^(int error) {
//			if (error) NSLog(@"Error in dispatch IO");
//			
//			close(descriptor);
//			
////			[self closeConnection];
//		});
		
		// Read from the IO Queue
//		dispatch_io_read(inQ, 0, SIZE_MAX, queue, ^(bool done, dispatch_data_t data, int error) {
//			
//			if (data)
//			{
//				dispatch_data_apply(data, ^bool(dispatch_data_t region, size_t offset, const void *buffer, size_t size) {
//					
//					NSData *localData = [NSData dataWithBytes:buffer length:size];
//					DMHttpRequest *request = [[DMHttpRequest alloc] initWithData:localData];
//					
//					NSData *response = [DMApiController respondToRequest:request];
//					dispatch_data_t dispatchData = dispatch_data_create(response.bytes, response.length, queue, ^{
//							
//					});
//					
//					NSFileHandle *handle = [[NSFileHandle alloc] initWithFileDescriptor:descriptor];
//					[handle writeData:response];
//					
//					return true;
//					
//					// Write out the response
//					dispatch_io_write(inQ, 0, dispatchData, queue, ^(bool done, dispatch_data_t data, int error) {
//						if (done) NSLog(@"Written Data");
//						
//						if (error) NSLog(@"Writing error");
//						
//						dispatch_io_close(inQ, 0);
//					});
//					
//					return true;
//				});
//			}
//			
//		});
		
		_handle = [[NSFileHandle alloc] initWithFileDescriptor:_descriptor];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(receiveMessage:)
													 name:NSFileHandleReadCompletionNotification
												   object:_handle];
		[_handle readInBackgroundAndNotify];
    }
	
    return self;
}

- (void) dealloc
{
//	[[NSNotificationCenter defaultCenter] removeObserver:self name:NSFileHandleReadCompletionNotification object:_handle];
	_handle = nil;
	_finishedWithConnection = nil;
}

@end
